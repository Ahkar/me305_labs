''' @file           Simon_Says.py
    @brief          Simulates a mini-game "Simon Says"
    @details        Implements turn base game using a finite state machine.
                    The game is played between the user and the Nucleo board, where the board generates a randomized sequence of blinks while the user try to reproduce the pattern.
                    User can play as many games as they like until they decides to exit.
    @author         Ahkar Kyaw
    @date           2/4/21
'''

# import sys
# import random
import pyb
import utime
from MorseCode import MorseCode

import micropython
micropython.alloc_emergency_exception_buf(100)

def onButtonPressFCN(line):
    """
    @brief          Record if and how the button is pressed.
    @param line     Specify which channel for the controller is being used.
    """
    global click
    global buttonPressed
    global pressTime
    global releaseTime

    if pinB1.value() == 0:
        pressTime = utime.ticks_us()
        buttonPressed = True
        #print('Button Pressed: ')
        #print(pressTime)
    elif pinB1.value() == 1:
        releaseTime = utime.ticks_us()
        buttonPressed = False
        #print('Button Released: ')
        #print(releaseTime)
        click = True
    else:
        # Program should never get here.
        pass

# Main program / test program begin
#   This code only runs if the script is executed as main by pressing play
#   but does not run if the scropt is imported as a module
if __name__ == '__main__':
    # Program initialization:
    state = 0 # Initial state
    click = False # Initial click
    buttonPressed = False
    delta_click = 0
    task1 = MorseCode()
    
    # Hardware initilization
    pinB1 = pyb.Pin(pyb.Pin.cpu.C13) # Button pin
    pinLED = pyb.Pin(pyb.Pin.cpu.A5) # LED pin
    ButtonPress = pyb.ExtInt(pinB1, pyb.ExtInt.IRQ_RISING_FALLING, pyb.Pin.PULL_NONE, onButtonPressFCN)
    
    # Game initilization
    # Is "dot" after holding for:
    dotTime=100
    # Is "dash" after holding for:
    dashTime=300
    # Separate as character after:
    charTime=500
    # Separate as word after:
    wordTime=1000
    
    computerDone = False
    playerDone = False
    
    while True:
        try:
            if state==0:
                # Run state(0) Welcome code
                print('\nWelcome!\nPress the Blue Button to start.\nPress Ctrl+C to exit.')
                # Until the flag is True, pass
                while not click:
                    pass
                state = 1 # change state
                match = 1 # start match 1
                difficulty = 1 # set difficulity to 1
                record = [] # create an empty array to track record
                click = False # unclick the button
                
                startTime = utime.ticks_us()
                index = 0
                
                # numberList = [111,222,333,444,555]
                # print("random item from list is: ", random.choice(numberList))
                
            elif state==1:
                # Run state(1) Computer's Trun
                print('\nS1 [Computer Playing]')
                print('Match ' + str(match))
                print('Round ' + str(difficulty))
                print('Score' + str(record))
                computerCode = 'AB' # code for the match
                print(computerCode)
                computerPattern = task1.encrypt(computerCode.upper()) # turn the code into morse code
                print(computerPattern)
                while not computerDone:
                    if click == True:
                        print('Computer is not done playing!')
                        click = False
                    else:
                        currentTime = utime.ticks_us()
                        deltaTime = utime.ticks_diff(currentTime,startTime)/1000
                        if index >= len(computerPattern):
                            computerDone = True
                        else:
                            if computerPattern[index] == '.':
                                # if deltaTime <= dotTime:
                                #     pinLED.high()
                                # elif dotTime < deltaTime < dotTime+dotTime:
                                #     pinLED.low()
                                # else:
                                #     index += 1
                                #     startTime = utime.ticks_us()
                                pass
                            elif computerPattern[index] == '-':
                                # if deltaTime <= charTime:
                                #     pinLED.high()
                                # elif charTime < deltaTime < charTime+dotTime:
                                #     pinLED.low()
                                # else:
                                #     index += 1
                                #     startTime = utime.ticks_us()
                                pass
                            elif computerPattern[index] == ' ':
                                # if deltaTime <= charTime+dotTime:
                                #     pinLED.low()
                                # else:
                                #     index += 1
                                #     startTime = utime.ticks_us()
                                pass
                            else:
                                # Program should never get here.
                                pass
                            print(index)
                            index += 1
                state = 2 # move to state 2
                computerDone = False # reset the computerDone tracker
            elif state==2:
                # Run state(2) Player's Turn
                print('\nS2 [Player Playing]')
                print('Start when you are ready')
                print(computerPattern)
                playerPattern = ""
                startTime = utime.ticks_us()
                while not playerDone:
                    if len(playerPattern) == len(computerPattern):
                        playerDone = True
                    else:
                        if buttonPressed == False:
                            if click == True:
                                pass
                            else:
                                currentTime = utime.ticks_us()
                                if playerPattern == "":
                                    pass
                                else:
                                    if utime.ticks_diff(currentTime, startTime)/1000 > charTime:
                                        playerPattern += ' '
                                        print('Player still Playing')
                                        print('Pressed for: ' + str(delta_click) + ' ms')
                                        print(computerPattern)
                                        print(playerPattern)
                                        #print(len(computerPattern))
                                        #print(len(playerPattern))
                                        startTime = utime.ticks_us()
                                    else:
                                        pass
                        if click == True:
                            print('Player still Playing')
                            delta_click = utime.ticks_diff(releaseTime,pressTime)/1000
                            print('Pressed for: ' + str(delta_click) + ' ms')
                            click = False
                            if delta_click <= dashTime:
                                playerPattern += '.'
                                print(computerPattern)
                                print(playerPattern)
                                #print(len(computerPattern))
                                #print(len(playerPattern))
                                delta_click = 0
                            elif delta_click > dashTime:
                                playerPattern += '-'
                                print(computerPattern)
                                print(playerPattern)
                                #print(len(computerPattern))
                                #print(len(playerPattern))
                                delta_click = 0
                            else:
                                # Program should never get here.
                                pass
                            startTime = utime.ticks_us()
                        else:
                            pass
                if playerPattern == computerPattern:
                    if difficulty == 3:
                        playerDone = False # reset the playerDone tracker
                        difficulty = 1
                        state = 3
                    else:
                        print('\nRound ' + str(difficulty) + ' Win')
                        print(computerPattern)
                        print(playerPattern)
                        playerDone = False # reset the playerDone tracker
                        difficulty += 1
                        state = 1
                        print('Difficulty Increased!!')
                    
                else:
                    print(computerPattern)
                    print(playerPattern)
                    state = 4 # move to state 4
                    playerDone = False # reset the playerDone tracker
                    
            elif state==3:
                # Run state(3) Player Win
                print('\nS3 [Player Win]')
                record.append(1)
                print(record)
                state = 5 # move to state 5
                
            elif state==4:
                # Run state(4) Player Lose
                print('\nS4 [Player Lose]')
                record.append(0)
                print('Match ' + str(match))
                print('Score' + str(record))
                state = 5 # move to state 5
                
            elif state==5:
                # Run state(5) Play Again?
                print('\nS5 [Play Again?]')
                print('Yes:"."\nNo: "-"')
                while not click:
                    pass
                click = False
                delta_click = utime.ticks_diff(releaseTime,pressTime)/1000
                if delta_click <= dashTime:
                    print('.')
                    state = 1
                    match += 1
                    difficulty = 1
                    delta_click = 0
                elif delta_click > dashTime:
                    print('-')
                    state = 6
                    delta_click = 0
                    
            elif state==6:
                # Run state(6) Display Results
                print('\nS6 [Display Results]')
                print('Score ' + str(record))
                while not click:
                    pass
                click = False
#               sys.exit("Game Over")
            else:
                # Program should never get here.
                pass

        except KeyboardInterrupt:
            # This except block catches "Ctrl-C" from the keyboard to end the
            # while(True) loop when desired
            print('Ctrl-C has been pressed')
            break

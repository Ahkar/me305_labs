''' @file           Simon_Says_class.py
    @brief          Simulates a mini-game "Simon Says"
    @details        Implements turn base game using a finite state machine.
                    The game is played between the user and the Nucleo board.
                    The board generates a randomized sequence of blinks while the user try to reproduce the pattern.
                    User can play as many games as they like until they decides to exit.
    @author         Ahkar Kyaw
    @author         David Hernandez
    @date           1/27/21
    @copyright      2020-2021 Kyaw & Hernandez
'''


import pyb
import utime
from random import choice
from MorseCode import MorseCode

# Helps debug
import micropython
micropython.alloc_emergency_exception_buf(100)

class Simon_Says_class:
    ''' @brief      Sets command for memory game
        @details    The commands are used by a task runner from another py file.
                    The class contains the game "Simon Says" and all the variables and function related to the game.
    '''
    
    # Static variables are defined outside of methods
    
    # State constants
    S0_INIT            = 0
    S1_COMPUTER_TURN   = 1
    S2_USER_TURN       = 2
    S3_USER_SUCCESS    = 3
    S4_USER_FAILED     = 4
    S5_CONTINUE        = 5
    S6_RESULTS         = 6
    
    ## @brief    Calls on MorseCode dictionary values
    alpha = ('A','B','C','D','E','F','G','H','I','J','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z','1','2','3','4','5','6','7','8','9','0',',','.','?','/','-','(',')')
    
    def __init__(self, dotTime=100, dashTime=200, charTime=500, slow=2, DBG_flag=True):
        ''' @brief          Initialization for the game
            @details        Assigning some variables for the game to work and allowing user optimization for the game
            @param dotTime  Tells game value is dot if pressed for less than dotTime
            @param dashTime Tells game value is a dash if pressed for longer than dashTime
            @param charTime Tells game value is a space after not pressed for charTime
            @param slow     Tells the game to slow down the blink speed
            @param DBG_flag Used to customize if the player want to see the debugging messages or not
        '''
        # Function initialization:
        
        ## Flag to specify if debug messages print
        self.DBG_flag = DBG_flag
        
        ##################################################
        
        # Program initialization:
        
        ## The current state of the finite state machine
        self.state = 0
        
        ## Initializes that button has not been clicked
        self.click = False
        ## Initializes that button has not been pressed
        self.buttonPressed = False
        
        ## Initializes time difference between button pressed/released
        self.delta_click = 0
        
        ## Calls on morse code class
        self.task = MorseCode()
        
        ## Initializes that computer has no started
        self.computerDone = False
        ## Initializes that player has no started
        self.playerDone = False
        
        ## Counts number of wins for player
        self.playerWin = 0
        ## Counts number of wins for computer
        self.computerWin = 0
        
        ##################################################
        
        # Game Initialization
        
        ## Tells game value is dot if less than dotTime
        self.dotTime = dotTime
        ## Tells game value is a dash if greater than dashTime
        self.dashTime = dashTime
        ## Tells game value is a space after not pressed for charTime
        self.charTime = charTime
        ## Tells the game to slow down the blink speed
        self.slow = slow
        ## Tells the game to slow down the blink speed by two times self.slow for dash
        self.slow_dash = self.slow*2
        
        ##################################################
        
        ## Hardware initialization:
        
        # Button pin initialization
        self.pinB1 = pyb.Pin(pyb.Pin.cpu.C13)
        ## @brief   Locates blue button on nucleo
        #  @details Associates further commands with this button
        #
        # Led pin initialization
        self.pinLED = pyb.Pin(pyb.Pin.cpu.A5)
        ## @brief   Locates LED on nucleo
        #  @details Associates further commands with this LED
        #
        ## Timer initialization
        self.tim2 = pyb.Timer(2, freq = 1000)
        ## @brief Sets timer channel for LED on nucleo
        #
        self.t2ch1 = self.tim2.channel(1, pyb.Timer.PWM, pin = self.pinLED)
        ## @brief Control pulse width percent of LED
        #
        self.ButtonPress = pyb.ExtInt(self.pinB1, pyb.ExtInt.IRQ_RISING_FALLING, pyb.Pin.PULL_NONE, callback=self.onButtonPressFCN)
        ## @brief    Sets the callback
        #  @details  The command interrupts current code and calls back function
    
    def run(self):
        ''' @brief      Sets command for memory game
            @details    The commands are used by a task runner from another py file.
                        The class contains the game "Simon Says" and all the variables and function related to the game.
        '''
        # main program code goes here
        
        if self.state==self.S0_INIT:
            # Run state(0) Welcome code
            print('\nWelcome!\nPress the Blue Button to start.\nPress Ctrl+C to exit.')
            # Until the flag is True, pass
            while not self.click:
                pass
            self.click = False # unclick the button
            ## Counter for the number of matchs played
            self.match = 1 # start match 1 which encompasses all rounds
            ## Counter for the difficulity, dictates how many letters per round
            self.difficulty = 1 # set difficulity to 1
            ## Counter for index inside computerPattern for computer to blink
            self.index = 0 # counter for the computer pattern
            ## Keep track of the initial time as a reference
            self.startTime = utime.ticks_us() # remember the time before state transition
            ## Randomly generated code for computer convert to morse code
            self.computerCode = "" # empty computerCode
            self.transitionTo(self.S1_COMPUTER_TURN) # Updating state for next iteration
            
        elif self.state==self.S1_COMPUTER_TURN:
            # Run state(1) Computer's Turn
            print('\nS1 [Computer Playing]')
            print('Match ' + str(self.match))
            print('Round ' + str(self.difficulty))
            print('Score:')
            print('    Player:   ' + str(self.playerWin))
            print('    Computer: ' + str(self.computerWin))
            # add random string into computerCode
            self.computerCode += choice(self.alpha) # code for the match
            ## Stores encrypted computerCode in MorseCode
            self.computerPattern = self.task.encrypt(self.computerCode.upper()) # turn the code into morse code
            # until the computer is done blinking everything, stay inside the while loop
            while not self.computerDone:
                # prevent user from clicking the button when computer is still playing
                if self.click:
                    self.click = False
                    print('Computer is not done playing!')
                # let the computer blink the led pattern
                else:
                    ## Record the currentTime
                    self.currentTime = utime.ticks_us() # check current time
                    ## Stores the difference between currentTime and startTime
                    self.deltaTime = utime.ticks_diff(self.currentTime,self.startTime)/1000 # compare currentTime with startTime to calculate deltaTime
                    # when computer finish blinking everything, note that it is done
                    if self.index >= len(self.computerPattern):
                        self.computerDone = True
                    else:
                        # blink "."
                        if self.computerPattern[self.index] == '.':
                            if self.deltaTime <= (self.dotTime)*self.slow:
                                self.t2ch1.pulse_width_percent(100)
                            elif (self.dotTime)*self.slow < self.deltaTime < (self.dotTime+self.dotTime)*self.slow:
                                self.t2ch1.pulse_width_percent(0)
                            else:
                                print(str(self.index) + ' ' + str(self.computerPattern[self.index]))
                                self.index += 1
                                self.startTime = utime.ticks_us() # reset the start time
                            pass
                        # blink "-"
                        elif self.computerPattern[self.index] == '-':
                            if self.deltaTime <= (self.dashTime)*self.slow_dash:
                                self.t2ch1.pulse_width_percent(100)
                            elif (self.dashTime)*self.slow_dash < self.deltaTime < (self.dashTime+self.dotTime)*self.slow_dash:
                                self.t2ch1.pulse_width_percent(0)
                            else:
                                print(str(self.index) + ' ' + str(self.computerPattern[self.index]))
                                self.index += 1
                                self.startTime = utime.ticks_us() # reset the start time
                            pass
                        # blink " "
                        elif self.computerPattern[self.index] == ' ':
                            if self.deltaTime <= (self.charTime+self.dotTime)*self.slow:
                                self.t2ch1.pulse_width_percent(0)
                            else:
                                print(str(self.index) + ' ' + str(self.computerPattern[self.index]))
                                self.index += 1
                                self.startTime = utime.ticks_us() # reset the start time
                            pass
                        else:
                            # Program should never get here.
                            pass
            self.computerDone = False # reset the computerDone tracker
            self.transitionTo(self.S2_USER_TURN) # Updating state for next iteration
            
        elif self.state==self.S2_USER_TURN:
            # Run state(2) Player's Turn
            print('\nS2 [Player Playing]')
            print('Start when you are ready')
            print(self.computerPattern)
            ## Record the input pattern by the player
            self.playerPattern = "" # empty user pattern
            self.startTime = utime.ticks_us() # reset the start time
            # if the palyer don't click
            while not self.playerDone:
                # when user finish playing, note that it is done
                if len(self.playerPattern) == len(self.computerPattern):
                    self.playerDone = True
                # until the user finish playing
                else:
                    # if button is not pressed
                    if self.buttonPressed == False:
                        self.t2ch1.pulse_width_percent(0) # turn the led off
                        if self.click == True:
                            pass
                        else:
                            self.currentTime = utime.ticks_us() # check current time
                            # don't do anything if player haven't start
                            if self.playerPattern == "":
                                pass
                            # if player have started but have not pressed anything for more than charTime, record it as a " "
                            else:
                                if utime.ticks_diff(self.currentTime, self.startTime)/1000 > self.charTime:
                                    self.playerPattern += ' ' # record that the player has pressed a " "
                                    print('Player still Playing')
                                    print('Pressed for: ' + str(self.delta_click) + ' ms')
                                    print(self.computerPattern)
                                    print(self.playerPattern)
                                    self.startTime = utime.ticks_us() # reset the start time
                                else:
                                    pass
                    # if button is pressed
                    else:
                        self.t2ch1.pulse_width_percent(100) # turn the led on
                    # if button has been clicked
                    if self.click == True:
                        self.click = False # unclick the button
                        self.t2ch1.pulse_width_percent(0) # turn the led off
                        self.delta_click = utime.ticks_diff(self.releaseTime,self.pressTime)/1000 # calculate how long the button has been pressed
                        print('Player still Playing')
                        print('Pressed for: ' + str(self.delta_click) + ' ms')
                        if self.delta_click <= self.dashTime:
                            self.playerPattern += '.' # record that the player has pressed a "."
                            self.delta_click = 0 # reset delta_click
                            print(self.computerPattern)
                            print(self.playerPattern)
                        elif self.delta_click > self.dashTime:
                            self.playerPattern += '-' # record that the player has pressed a "-"
                            self.delta_click = 0 # reset delta_click
                            print(self.computerPattern)
                            print(self.playerPattern)
                        else:
                            # Program should never get here.
                            pass
                        self.startTime = utime.ticks_us() # reset the start time
                    else:
                        pass
            if self.playerPattern == self.computerPattern:
                # after round 3, the player win the match
                if self.difficulty == 3:
                    self.playerDone = False # reset the playerDone tracker
                    self.difficulty = 1 # reset the round
                    self.transitionTo(self.S3_USER_SUCCESS) # Updating state for next iteration
                else:
                    print('\nRound ' + str(self.difficulty) + ' Win')
                    print(self.computerPattern)
                    print(self.playerPattern)
                    print('Difficulty Increased!!')
                    self.playerDone = False # reset the playerDone tracker
                    self.difficulty += 1 # increase difficulty for the next round
                    self.index = 0 # counter for the computer pattern
                    self.transitionTo(self.S1_COMPUTER_TURN) # Updating state for next iteration

            else:
                print(self.computerPattern)
                print(self.playerPattern)
                self.playerDone = False # reset the playerDone tracker
                self.transitionTo(self.S4_USER_FAILED) # Updating state for next iteration
            
        elif self.state==self.S3_USER_SUCCESS:
            # Run state(3) Player Win
            print('\nS3 [Player Win]')
            self.playerWin += 1 # Add one to the number of matches the player wins
            print('Match ' + str(self.match))
            print('Round ' + str(self.difficulty))
            print('Score:')
            print('       Player:   ' + str(self.playerWin))
            print('       Computer: ' + str(self.computerWin))
            self.transitionTo(self.S5_CONTINUE) # Updating state for next iteration
            
        elif self.state==self.S4_USER_FAILED:
             # Run state(4) Player Lose
            print('\nS4 [Player Lose]')
            self.computerWin += 1 # Counts the amount of match losses
            print('Match ' + str(self.match))
            print('Round ' + str(self.difficulty))
            print('Score:')
            print('       Player:   ' + str(self.playerWin))
            print('       Computer: ' + str(self.computerWin))
            self.transitionTo(self.S5_CONTINUE)   # Updating state for next iteration
            
        elif self.state==self.S5_CONTINUE:
            # Run state(5) Play Again?
            print('\nS5 [Play Again?]')
            print('Yes:"."\nNo: "-"')
            while not self.click:
                pass
            self.click = False # unclick the button
            self.delta_click = utime.ticks_diff(self.releaseTime,self.pressTime)/1000 # calculate how long the button has been pressed
            if self.delta_click <= self.dashTime:
                print('.')
                self.delta_click = 0 # reset delta_click
                self.match += 1 # note that it is a new match
                self.difficulty = 1 # reset difficulty
                self.index = 0 # counter for the computer pattern
                self.computerCode = "" # empty computerCode
                self.startTime = utime.ticks_us() # remember the time before state transition
                self.transitionTo(self.S1_COMPUTER_TURN) # Updating state for next iteration
            elif self.delta_click > self.dashTime:
                print('-')
                self.delta_click = 0 # reset delta_click
                self.transitionTo(self.S6_RESULTS) # Updating state for next iteration
                
        elif self.state==self.S6_RESULTS:
            # Run state(6) Display Results
            print('\nS6 [Display Results]')
            print ('Score:'
                   ' Player {:} Computer {:}.'.format(self.playerWin,self.computerWin))
            while not self.click:
                pass
            self.click = False # unclick the button
        else:
            # Program should never get here.
            pass
    
    def transitionTo(self, newState):
        """ @brief          Function to help change state with a debug funciton to diaplay status.
            @param NewState Input variable, new State that we want to move to.
        """
        # If Debug is on, display the change of state
        if self.DBG_flag:
            print("\nS" + str(self.state) + " -> S" + str(newState))
        self.state = newState # update the state to newState
    
    def onButtonPressFCN(self,line):
        """ @brief          Record if and how the button is pressed.
            @param line     Specify which channel for the controller is being used.
        """
        global click
        global buttonPressed
        global pressTime
        global releaseTime
        
        # when button is pressed
        if self.pinB1.value() == 0:
            ## Record the time at which the button has been pressed
            self.pressTime = utime.ticks_us() # remember the time at which the button is pressed
            self.buttonPressed = True # note that button is pressed
        # when button is released
        elif self.pinB1.value() == 1:
            ## Record the time at which the button has been released
            self.releaseTime = utime.ticks_us() # remember the time at which the button is released
            self.buttonPressed = False # note that button is released
            self.click = True # note that button has been clicked
        else:
            # Program should never get here.
            pass

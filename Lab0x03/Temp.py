# -*- coding: utf-8 -*-
"""
Created on Thu Feb  4 12:36:20 2021

@author: 16507
"""

from MorseCode import MorseCode

if __name__ == "__main__":
    # Program initialization goes here
    task1 = MorseCode()
    
    message = "Howdy"
    result = task1.encrypt(message.upper())
    print(result)
    
    message = ".... . .-.. .-.. --- -....- .... . .-.. .-.. --- -....- .... --- .-- -....- .- .-. . -....- -.-- --- ..- -....- -.. --- .. -. --. ..--.."
    result = task1.decrypt(message) 
    print (result) 
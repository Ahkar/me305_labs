''' @file           MorseCode.py
    @brief          Encrypt and decrypt morse code
    @details        Python program to implement Morse Code Translator
                    \nSource: https://www.geeksforgeeks.org/morse-code-translator-python/
'''

# VARIABLE KEY
# 'cipher' -> 'stores the morse translated form of the english string'
# 'decipher' -> 'stores the english translated form of the morse string'
# 'citext' -> 'stores morse code of a single character'
# 'i' -> 'keeps count of the spaces between morse characters'
# 'message' -> 'stores the string to be encoded or decoded'


class MorseCode:
    ''' @brief      Morse Code Dictionary
        @details    The program use the dictionary to encrypt and decrypt the input morse code
    '''
    def __init__(self):
        # Dictionary representing the morse code chart
        self.MORSE_CODE_DICT = { 'A':'.-', 'B':'-...',
                                 'C':'-.-.', 'D':'-..', 'E':'.',
                                 'F':'..-.', 'G':'--.', 'H':'....',
                                 'I':'..', 'J':'.---', 'K':'-.-',
                                 'L':'.-..', 'M':'--', 'N':'-.',
                                 'O':'---', 'P':'.--.', 'Q':'--.-',
                                 'R':'.-.', 'S':'...', 'T':'-',
                                 'U':'..-', 'V':'...-', 'W':'.--',
                                 'X':'-..-', 'Y':'-.--', 'Z':'--..',
                                 '1':'.----', '2':'..---', '3':'...--',
                                 '4':'....-', '5':'.....', '6':'-....',
                                 '7':'--...', '8':'---..', '9':'----.',
                                 '0':'-----', ', ':'--..--', '.':'.-.-.-',
                                 '?':'..--..', '/':'-..-.', '-':'-....-',
                                 '(':'-.--.', ')':'-.--.-'}

    # Function to encrypt the string
    # according to the morse code chart
    def encrypt(self, input):
        self.message = input
        self.cipher = ''
        for letter in self.message:
            if letter != ' ':
                # Looks up the dictionary and adds the
                # correspponding morse code
                # along with a space to separate
                # morse codes for different characters
                self.cipher += self.MORSE_CODE_DICT[letter] + ' '
            else:
                # 1 space indicates different characters
                # and 2 indicates different words
                self.cipher += ' '
        return self.cipher

    # Function to decrypt the string
    # from morse to english
    def decrypt(self, input):
        self.message = input
        # extra space added at the end to access the
        # last morse code
        self.message += ' '
        self.decipher = ''
        self.citext = ''
        for letter in self.message:
            # checks for space
            if (letter != ' '):
                # counter to keep track of space
                i = 0
                # storing morse code of a single character
                self.citext += letter
            # in case of space
            else:
                # if i = 1 that indicates a new character
                i += 1
                # if i = 2 that indicates a new word
                if i == 2 :
                    # adding space to separate words
                    self.decipher += ' '
                else:
                    # accessing the keys using their values (reverse of encryption)
                    self.decipher += list(self.MORSE_CODE_DICT.keys())[list(self.MORSE_CODE_DICT.values()).index(self.citext)]
                    self.citext = ''
        return self.decipher
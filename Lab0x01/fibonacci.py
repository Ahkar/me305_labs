# -*- coding: utf-8 -*-
"""
Created on Thu Jan 14 08:26:02 2021

@author: Ahkar
@file: fibonacci.py
"""

def fib(idx):
  """
  @brief      This function calculates a Fibonacci number at a specific index.
  @param idx  An integer specifying the index of the desired
              Fibonacci number
  """
  x = [] 
  for i in range(idx+1):
      if i == 0:
          x.append(0)
      elif i == 1:
          x.append(1)
      else:
          x.append(x[i-1]+x[i-2])   
  return x[idx]

def fibneg(idx):
  """
  @brief      This function calculates a Fibonacci number at a specific index.
  @param idx  An integer specifying the index of the desired
              Fibonacci number
  """
  x = [] 
  for i in range(idx+1):
      if i == 0:
          x.append(0)
      elif i == 1:
          x.append(1)
      else:
          x.append((x[i-1]+x[i-2]))
  for i in range (0,idx+1,2):
      x[i] = -x[i]
  return x[idx]

if __name__ == '__main__':
    try:    
        while True:
            fib_index = input('Please enter an index to recieve a Fibonacci number! \n \t idx: ')
            if fib_index[0]=="-":
                if fib_index[1:].isdigit():
                    idx=int(fib_index[1:])
                    print('Fibonacci number at '
                          'index -{:} is {:}.'.format(idx,fibneg(idx)))
                else:
                    print('Only integers are accepted')   
            elif fib_index.isdigit():
                idx=int(fib_index)
                print('Fibonacci number at '
                      'index {:} is {:}.'.format(idx,fib(idx)))
            else:
                print('Only integers are accepted')
    except KeyboardInterrupt:
        print('\nSee you next time!')


'''
@file       LED_Pattern.py
@brief      Cycle through 3 LED Patterns with a button
@details    Implements a finite state machine that cycles through 3 LED Patterns,
            (Square Pattern, Sine Pattern, Sawtooth Pattern) when a button is pressed.
@image      html LED_Pattern_State-transition_diagram.png "LED Pattern State-transition diagram" width=50%
@author     Ahkar Kyaw
@date       1/21/2021
'''

import pyb
import time
import math
from pyb import Pin
from pyb import ExtInt

import micropython
micropython.alloc_emergency_exception_buf(100)

def onButtonPressFCN(line):
    """
    @brief          Assign True to flag when button is pressed.
    """
    global flag
    flag = True

# Main program / test program begin
#   This code only runs if the script is executed as main by pressing play
#   but does not run if the scropt is imported as a module
if __name__ == '__main__':
    # Program initialization:
    state = 0 # Initial state
    flag = False # Initial flag
    # Pin initilization
    pinB1 = Pin(pyb.Pin.cpu.C13) # Button pin
    pinLED = Pin (pyb.Pin.cpu.A5) # LED pin
    tim2 = pyb.Timer(2, freq = 1000) # Create a timer object using timer 1 - trigger at 1000Hz
    t2ch1 = tim2.channel(1, pyb.Timer.PWM, pin=pinLED) # Create channel 1 using tim2
    ButtonInt = ExtInt(pinB1, ExtInt.IRQ_FALLING, Pin.PULL_NONE, onButtonPressFCN) # Configure pinB1 to interrupt the program and run a callback function
    
    while True:
        # Main program code:
        currentTime = time.ticks_ms() # Check current time
        if state==0:
            # Run state(0) Welcome code
            print('\nWelcome! \nPress the blue button to cycle through the LED patterns. \nPress Ctrl+C to exit.')
            # Until the flag is True, pass
            while not flag:
                pass
            # Move to State 1
            state = 1
            print('\nSquare wave pattern selected\n')
            flag = False
            start = time.ticks_ms() # Restart start time
            
        elif state==1:
            # Run state(1) Square Wave Pattern code
            print('S1 [Square wave pattern]')
            # Move to state 2 if button is pressed
            if flag == True:
                state = 2
                print('\nSine wave pattern selected\n')
                flag = False
                start = time.ticks_ms() # Restart start time
            delta = time.ticks_diff(currentTime,start)/1000 # Calculate the time difference from the start time [s]
            intensity = 100*(delta%1 < 0.5) # If delta is less than 0.5s, intensity = 100; Else intensity = 0.
            t2ch1.pulse_width_percent(intensity) # Adjust LED brightness based on intensity
            
        elif state==2:
            # Run state(2) Sine Pattern code
            print('S2 [Sine wave pattern]')
            # Move to state 3 if button is pressed
            if flag == True:
                state = 3
                print('\nSawtooth wave pattern selected\n')
                flag = False
                start = time.ticks_ms() # Restart start time
            else:
                delta = time.ticks_diff(currentTime,start)/1000 # Calculate the time difference from the start time [s]
                intensity = 50*(math.sin(math.pi*delta/5)+1) # Equation for desired sine wave
                t2ch1.pulse_width_percent(intensity) # Adjust LED brightness based on intensity
                
        elif state==3:
            # Run state(3) Sawtooth Wave Pattern code
            print('S3 [Sawtooth wave pattern]')
            # Move to state 1 if button is pressed
            if flag == True:
                state = 1
                print('\nSquare wave pattern selected\n')
                flag = False
                start = time.ticks_ms() # Restart start time
            else:
                delta = time.ticks_diff(currentTime,start)/1000 # Calculate the time difference from the start time [s]
                intensity = 100*(delta%1) # Equation for desired sawtooth pattern
                t2ch1.pulse_width_percent(intensity) # Adjust LED brightness based on intensity
                
        else:
            # Program should not get here.
            pass
''' @file       CON_task2.py
    @brief      Control the EncoderDriver class using finite state machine
    @details    Based on the state in the shared folder, 
                carry out tasks that communicate with the EncoderDriver class.
    @author     Ahkar Kyaw
    @date       2/18/21
    @copyright  2020-2021 Kyaw
'''

import pyb
from pyb import Timer, Pin
import shares
from EncoderDriver2 import EncoderDriver

class CON_task:
    
    # Constants initializationz
    # State constants
    S0_INIT      = 0
    S1_PRESSED_Z = 1
    S2_PRESSED_P = 2
    S3_PRESSED_D = 3
    S4_PRESSED_G = 4
    S5_PRESSED_S = 5
    
    def __init__(self,PIN1,PIN2,TIM):
        ''' @brief      A class that connects the EncoderDriver, MotorDriver and ClosedLoop together
            @details    Initialize the controller task which will be used to communicate with the three driver classes.
        '''
        
        # Function initialization:
        
        ##################################################
        
        # Program initialization:
        ## Local counter variable
        self.n = 0
        ## Variable to store the reset value
        self.resetPosition = 0
        ## Variable that store the period of the encoder
        self.Period = 65535
        
        ## Variable to store the position data
        self.deltaPosition = 0
        ##################################################
        
        # Hardware initialization:
        pyb.repl_uart(None) # prevent print command from printing in Spyder
        
        ## Stores the first input pin
        self.Pin1 = Pin(PIN1)
        ## Stores the second input pin
        self.Pin2 = Pin(PIN2)
        
        # Encoder
        ## Stores the timer variable
        self.TIM = Timer(TIM, period=0xFFFF, prescaler=0)
        # self.TIM.channel(1, mode=Timer.ENC_AB, pin=self.Pin1)
        # self.TIM.channel(2, mode=Timer.ENC_AB, pin=self.Pin2)
        
        # Encoder_driver
        ## Encoder driver class variable
        self.Enc = EncoderDriver(self.Pin1, self.Pin2, TIM, self.Period)
        
        ##################################################
        
        # initialization message
        print("CON_task Initialized.")
        
    def run(self):
        ''' @brief      Runs the finite-state machine
            @details    Comprises of different states for Encoder, Motor and Closedloop
        '''
        # main program code goes here
        
        ## Stores the current position of the encoder to be used across different task
        shares.currentPosition = self.Enc.update(shares.currentPosition) # update the encoder position
        
        if shares.CONState == self.S0_INIT:
            pass
        
        elif shares.CONState == self.S1_PRESSED_Z:
            shares.currentPosition = self.Enc.set_position(self.resetPosition)
            if self.n%5000>=4999:
                print('Current position        : '+str(shares.currentPosition)) # print the current position at a slower interval
            self.n+=1
            
        elif shares.CONState == self.S2_PRESSED_P:
            shares.currentPosition = self.Enc.get_position() # get the current position of the encoder
            
        elif shares.CONState == self.S3_PRESSED_D:
            self.deltaPosition = self.Enc.get_delta()
            if self.n%5000>=4999:
                print('Corrected delta position: '+str(self.deltaPosition)) # print the current speed at a slower interval
            self.n+=1
            
        if shares.UIState == self.S0_INIT:
            pass
        elif shares.UIState == self.S4_PRESSED_G:
            pass
        elif shares.UIState == self.S5_PRESSED_S:
            pass
        

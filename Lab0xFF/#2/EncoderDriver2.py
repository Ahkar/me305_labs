''' @file       EncoderDriver2.py
    @brief      Obtain data from the encoder and carry out calculation
    @details    A class that will be used within CON_task to control and interact with the encoder.
                This class has 4 functions, update, set_position, get_position and get_delta.
    @author     Ahkar Kyaw
    @date       2/18/21
    @copyright  2020-2021 Kyaw
'''

import pyb
from pyb import Pin, Timer

class EncoderDriver:
    
    def __init__(self,PIN1, PIN2, TIM, PERIOD):
        ''' @brief          Initialize the Encoder driver class
            @details        A class that connects the back end task with the hardware to collect data
            @param PIN1     Input Pin value for the first pin
            @param PIN2     Input Pin value for the second pin
            @param TIM      Input Timer for the encoder
            @param PERIOD   Input Period for the encoder
        '''
        
        # Hardware initialization:
        pyb.repl_uart(None) # prevent print command from printing in Spyder
        
        ## Stores the first input pin
        self.Pin1 = Pin(PIN1)
        ## Stores the second input pin
        self.Pin2 = Pin(PIN2)
        
        ## Stores the timer variable
        self.TIM = Timer(TIM, period=0xFFFF, prescaler=0)
        self.TIM.channel(1, mode=Timer.ENC_AB, pin=self.Pin1) # Create a timer channel for the first pin
        self.TIM.channel(2, mode=Timer.ENC_AB, pin=self.Pin2)# Stores the timer channel for the first pin
        
        ##################################################
        
        # Program initialization:
        ## Variable to store the current position of the encoder
        self.currentPosition = 0
        ## Array that store the current counter value and the previous counter value
        self.Counter = [0,0]
        ## Variable to store the calculated delta counter value
        self.deltaCounter = 0
        ## Variable to store the correction data for the delta value
        self.deltaCorrection = 0
        ## Variable that store the period of the encoder
        self.Period = PERIOD
        
    def update(self, oldPosition):
        ''' @brief              Updates the position of the encoder based on previous position
            @details            Function that updates the encoder position based on its previous position
            @param oldPosition  Previous position of the encoder
        '''
        
        self.Counter.pop(0) # remove the first position of the Counter array
        self.Counter.append(self.TIM.counter()) # append the new counter value to the Counter array
        
        self.deltaCounter = self.Counter[1] - self.Counter[0] # Calculating the difference between the two counter values
        
        # Correcting deltaCounter
        if -0.5*self.Period <= self.deltaCounter <= 0.5*self.Period:
            self.deltaCorrection = self.deltaCounter
        elif self.deltaCounter > 0.5*self.Period:
            self.deltaCorrection = self.deltaCounter - self.Period
        elif self.deltaCounter < -0.5*self.Period:
            self.deltaCorrection = self.deltaCounter + self.Period
        
        self.newPosition = oldPosition + self.deltaCorrection # store the new corrected position
        
        return self.newPosition # return the corrected position
        
    def set_position(self,resetPosition):
        ''' @brief                  Reset the position of the encoder to the specified value
            @param resetPosition    Position value that the user wants to be set to
        '''
        return resetPosition
        
    def get_position(self):
        ''' @brief                  Give the current position of the encoder
        '''
        return self.Counter[1]
        
    def get_delta(self):
        ''' @brief                  Give the current velocity of the encoder
        '''
        self.delta = self.deltaCounter
        return self.delta
        
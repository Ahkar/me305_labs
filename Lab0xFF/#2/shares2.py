''' @file       shares2.py
    @brief      Stores variables to be used across different tasks
    @details    Act as a global variable reserve so that different tasks can access the variables
    @author     Ahkar Kyaw
    @date       2/18/21
    @copyright  2020-2021 Kyaw
'''

## Variable to store the state variable for UI_task
UIState = 0
## Variable to store the state variable for CON_task
CONState = 0
## Variable to store the currentPosition of the shaft
currentPosition = 0

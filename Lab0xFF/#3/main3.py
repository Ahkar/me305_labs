# -*- coding: utf-8 -*-
''' @file       main3.py
    @brief      A file that runs the task files on the Nucleo
    @details    A default file that the Nucleo run when called.
    @author     Ahkar Kyaw
    @date       3/4/21
    @copyright  2020-2021 Kyaw
'''

import shares
from UI_task3 import UI_task
from CON_task3 import CON_task

# Main program / test program begin
#   This code only runs if the script is executed as main by pressing play
#   but does not run if the script is imported as a a module
if __name__ == "__main__":
    # Program initialization goes here
    task1 = UI_task()
    task2 = CON_task()
    
    # initialization message
    print("\nWelcome!")
    print("Press 'z' to zero the encoder 1 position")
    print("Press 'p' to print out the encoder 1 position")
    print("Press 'd' to print out the encoder 1 delta")
    print("Press 'g' to collect encoder 1 data for 30 seconds and generate a plot")
    print("Press 's' to end data collection prematurely")
    
    while True:
        try:
            task1.run() # run task 1
            
            task2.run() # run task 2
            
        except KeyboardInterrupt:
            # This except block catches "Ctrl-C" from the keyboard to end the
            # while(True) loop when desired
            print('Ctrl-c has been pressed')
            break
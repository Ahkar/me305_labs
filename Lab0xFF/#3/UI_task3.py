''' @file       UI_task3.py
    @brief      Read inputs, collect and return data
    @details    A Nucleo program that read input keyboard data from the computer
                and execute command.
                Here is a snapshot of the finite state machine:
                \image html ME305_Lab0xFF.jpg width=800cm height=600cm
                See file here:
                    https://bitbucket.org/Ahkar/me305_labs/src/master/Lab0xFF/
    @author     Ahkar Kyaw
    @date       2/4/21
    @copyright  2020-2021 Kyaw
'''

import pyb
import utime
import shares
from pyb import UART
from array import array

class UI_task:
    
    # Constants initializationz
    # State constants
    S0_INIT      = 0
    S1_PRESSED_Z = 1
    S2_PRESSED_P = 2
    S3_PRESSED_D = 3
    S4_PRESSED_G = 4
    S5_PRESSED_S = 5
    
    def __init__(self):
        ''' @brief      A class that connects the User Interface and the backend
            @details    Initialize the UI task which will be used to communicate between the UI_front and the CON_task.
        '''
        # Program initialization
        ## Counter for the class
        self.n = 0
        ## An array for time with initial time value of 0s
        self.t = array('f',[])
        ## An array for f(t) with initial value calculated at time = 0s
        self.y = array('f',[])
        ## Time Step for data collection [ms]
        self.stepTime = 50
        ## Tolerance for data collection stepTime [ms]
        self.stepTol = 1
        ## Lower range value for time data colleciton
        self.minStep = self.stepTime-self.stepTol
        ## Upper range value for time data colleciton
        self.maxStep = self.stepTime+self.stepTol
        
        # Hardware initialization
        self.myuart = UART(2) ## Store UART object for serial communications protocol
        pyb.repl_uart(None) # prevent print command from printing in Spyder
        
        # initialization message
        print("UI_task Initialized.")
        
    def run(self):
        ''' @brief      Runs the finite-state machine
            @details    Comprises of different states for UIState, ENCState and MOTState
        '''
        # main program
        if shares.ENCState == self.S0_INIT:
            if self.myuart.any() != 0:
                self.keyboardInput() # determine what the keybaoard input is and act accordingly
            
        elif shares.ENCState == self.S1_PRESSED_Z:
            if self.myuart.any() != 0:
                self.keyboardInput() # determine what the keybaoard input is and act accordingly
            
        elif shares.ENCState == self.S2_PRESSED_P:
            if self.myuart.any() != 0:
                self.keyboardInput() # determine what the keybaoard input is and act accordingly
            
        elif shares.ENCState == self.S3_PRESSED_D:
            if self.myuart.any() != 0:
                self.keyboardInput() # determine what the keybaoard input is and act accordingly
        
        if shares.UIState == self.S0_INIT:
            if self.myuart.any() != 0:
                self.keyboardInput() # determine what the keybaoard input is and act accordingly
                
        elif shares.UIState == self.S4_PRESSED_G:
            if self.myuart.any() != 0:
                self.keyboardInput() # determine what the keybaoard input is and act accordingly
                
            self.currentTime = utime.ticks_ms()
            self.deltaTime = utime.ticks_diff(self.currentTime,self.startTime)
            if self.deltaTime >= 30000:
                print('30s worth of data collected.')
                self.UI_transitionTo(self.S5_PRESSED_S) # updating state for next iteration
            elif self.minStep <= self.deltaTime%self.stepTime <= self.maxStep:
                print('deltaTime = '+str(self.deltaTime))
                self.t.append(self.deltaTime/1000)
                self.y.append(shares.currentPosition)
            pass
        
        elif shares.UIState == self.S5_PRESSED_S:
            if self.myuart.any() != 0:
                self.keyboardInput() # determine what the keybaoard input is and act accordingly
            print('Sending Data...')
            while self.n!=len(self.t):
                self.myuart.write('{:},{:}\r\n'.format(self.t[self.n],self.y[self.n]))
                self.n+=1
            self.myuart.write('Done')
            print('Data Sent')
            self.UI_transitionTo(self.S0_INIT) # updating state for next iteration
            pass
        

    def keyboardInput(self):
        """ @brief          Function to determine the keyboard input and change state accordingly
        """
        self.val = self.myuart.readchar() # store input from Spyder
        if self.val == 90 or self.val == 122: # if input is 'Z' or 'z'
            self.ENC_transitionTo(self.S1_PRESSED_Z) # updating state for next iteration
        elif self.val == 80 or self.val == 112: # if input is 'P' or 'p'
            self.ENC_transitionTo(self.S2_PRESSED_P) # updating state for next iteration
        elif self.val == 68 or self.val == 100: # if input is 'D' or 'd'
            self.ENC_transitionTo(self.S3_PRESSED_D) # updating state for next iteration
        elif self.val == 71 or self.val == 103: # if input is 'G' or 'g'
            self.startTime = utime.ticks_ms() # store the start time in which data collection begins
            self.UI_transitionTo(self.S4_PRESSED_G) # updating state for next iteration
        elif self.val == 83 or self.val == 115: # if input is 'S' or 's'
            self.UI_transitionTo(self.S5_PRESSED_S) # updating state for next iteration
        else:
            pass
        
    def UI_transitionTo(self, newState):
        """ @brief          Function to help change state of the UI_task related class
            @param newState Input variable, new State that we want to move to.
        """
        print('S{:}->S{:}'.format(shares.UIState,newState)) # print change of state in PuTTY
        shares.UIState = newState # update the UIState in shares file to newState
        
    def ENC_transitionTo(self, newState):
        """ @brief          Function to help change state of the CON_task related class
            @param newState Input variable, new State that we want to move to.
        """
        print('S{:}->S{:}'.format(shares.ENCState,newState)) # print change of state in PuTTY
        shares.ENCState = newState # update the UIState in shares file to newState
        


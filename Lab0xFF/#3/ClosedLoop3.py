''' @file       ClosedLoop3.py
    @brief      Used to correct the PWM to the desired rpm value
    @details    A class that helps to adjust the angular speed of the shaft
                to the desired reference velocity using a specific gain value
    @author     Ahkar Kyaw
    @date       3/4/21
    @copyright  2020-2021 Kyaw
'''

import pyb

class ClosedLoop:

    def __init__ (self):
        ''' @brief      Adjust the PWM value to achieve the desired angular velocity
            @details    A class that will be called using the CON_task to change the velocity of the shaft to match the reference velocity
            @param PWM  Pulse-width modulation value
        '''
        # Hardware initialization:
        pyb.repl_uart(None) # prevent print command from printing in Spyder
        
        ## Variable to store the PWM value
        self.PWM = 50
        ## Variable to store the Gain value
        self.Kp = 0
        ## Variable to store the new PWM value
        self.deltaPWM = 0
        
    def update(self,omega_ref,omega):
        ''' @brief              Updates the PWM value to get closer to the desired velocity
            @details            A class that will be called regularly in the CON_task to update the velocity of the shaft to match the reference velocity
            @param omega_ref    Input variable that states the desired velocity
            @param omega        Input variable that states the current velocity
        '''
        self.deltaPWM = self.Kp*(omega_ref-omega)
        if -100 <= self.deltaPWM <= 100:
            self.PWM += self.deltaPWM
            return self.PWM
        else:
            print('Desired PWM Reached')
    
    def get_Kp(self):
        ''' @brief      Return Kp value
            @detail     A function used tell the current gain value to the user
        '''
        return self.Kp
    
    def set_Kp(self,newKp):
        ''' @brief      Set Kp value
            @detail     A function used set a new gain value for calculation
        '''
        self.Kp = newKp
        return self.Kp
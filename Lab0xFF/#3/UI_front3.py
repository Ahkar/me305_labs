''' @file       UI_front3.py
    @brief      Using Nucleo board to calculate a funciton with respect to time from the computer
    @details    Send commands to the Nucleo board to record and report data. 
                Using the data recieved from the Nucleo board, plot a graph and generate a .CSV file
    @author     Ahkar Kyaw
    @date       2/18/21
    @copyright  2020-2021 Kyaw
'''

import csv
import serial
import keyboard
from array import array
from matplotlib import pyplot


def kb_cb(key):
    """ Callback function which is called when a key has been pressed.
    """
    global last_key
    last_key = key.name # Store keyboard input into last_key variable

def write_csv(file,x,y):
    """ Function to write data in .CSV file
    """
    n = 0
    with open('Lab0xFF_Data.csv','w',newline='') as data:
        data = csv.writer(data, delimiter = ',', quotechar = '|', quoting = csv.QUOTE_MINIMAL)
        for point in x:
            data.writerow([str(x[n]),str(y[n])])
            n += 1
    pass

# Tell the keyboard module to respond to these particular keys only
keyboard.on_release_key("Z", callback=kb_cb)#, suppress = True)
keyboard.on_release_key("P", callback=kb_cb)#, suppress = True)
keyboard.on_release_key("D", callback=kb_cb)#, suppress = True)
keyboard.on_release_key("G", callback=kb_cb)#, suppress = True)
keyboard.on_release_key("S", callback=kb_cb)#, suppress = True)

# Main program / test program begin
#   This code only runs if the script is executed as main by pressing play
#   but does not run if the script is imported as a a module
if __name__ == "__main__":
    # Program initialization
    ## Store the last input key from the Keyboard
    last_key = None
    ## Store data sent from Nucleo
    dataReceived = []
    ## Variable to keep track of loop
    n = 0
    ## Store data points for horizontal axis
    t = array('f', [])
    ## Store data points for vertical axis
    y = array('f', [])
    
    # Hardware initialization
    ## Open serial port to communicate with the Nucleo
    ser = serial.Serial(port='COM4',baudrate=115273,timeout=1)
    
    print("Welcome!")
    print("Press 'z' to zero the encoder 1 position")
    print("Press 'p' to print out the encoder 1 position")
    print("Press 'd' to print out the encoder 1 delta")
    print("Press 'g' to collect encoder 1 data for 30 seconds and generate a plot")
    print("Press 's' to end data collection prematurely")
    
    while True:
        try:
            if last_key is not None: # when user press something valid from keyboard
                print("You pressed " + last_key)
                ser.write(str(last_key).encode('ascii')) # send the keyboard key to Nucleo
                last_key = None # reset last_key
            ## Store the data sent from the Nucleo
            value = ser.readline().decode()
            if value == 'Done': # if Nucleo finish collecting data
                ser.close()
                break
            elif value == '': # if Nucleo has not started collecting data
                pass
            else:
                value = value.strip() # strip the value from Nucleo of white space
                dataReceived.append(value) # store it in dataReceived array
                
        except KeyboardInterrupt:
            # This except block catches "Ctrl-C" from the keyboard
            # to end the while(True) loop when desired
            print('Ctrl+c has been pressed')
            break

print('Data Received')

# Splits the developed data into two columns in order to plot
for i in range(len(dataReceived)):
        ## Store stripped data, one at a time
        dataStripped = dataReceived[i]
        ## Store splitted data, one at a time
        dataSplitted = dataStripped.split(',') # split data at the ','
        t.append(float(dataSplitted[0])) # append each of the t value from Nucleo to t
        y.append(float(dataSplitted[1])) # append each of the y value from Nucleo to y

print('Data Processed')

# Plotting data
pyplot.figure() # create a figure
pyplot.plot(t,y) # plot t and y
pyplot.xlabel('t (s)') # label x-axis
pyplot.ylabel('Position') # label y-axis


## Send data to csv file
f = open('Lab0xFF_Data.csv', "x")
write_csv('Lab0xFF_Data.csv',t,y)

# Turn off the callbacks so next time we run things behave as expected
keyboard.unhook_all()

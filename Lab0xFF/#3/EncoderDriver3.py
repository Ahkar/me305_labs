''' @file       Encoder_driver3.py
    @brief      Obtain data from the encoder and carry out calculation
    @details    A class that will be used within CON_task to control and interact with the encoder.
                This class has 4 functions, update, set_position, get_position and get_delta.
    @author     Ahkar Kyaw
    @date       3/18/21
    @copyright  2020-2021 Kyaw
'''

import utime
import pyb
from pyb import Pin, Timer

class EncoderDriver:
    
    def __init__(self,PIN1, PIN2, TIM, PERIOD):
        ''' @brief          Initialize the Encoder driver class
            @details        A class that connects the back end task with the hardware to collect data
            @param PIN1     Input Pin value for the first pin
            @param PIN2     Input Pin value for the second pin
            @param TIM      Input Timer for the encoder
            @param PERIOD   Input Period for the encoder
        '''
        
        
        # Hardware initialization:
        pyb.repl_uart(None) # prevent print command from printing in Spyder
        
        ## Stores the timer variable
        self.TIMR = Timer(TIM, period=PERIOD, prescaler=0)
        self.TIMR.channel(1, mode=Timer.ENC_AB, pin=Pin(PIN1))
        self.TIMR.channel(2, mode=Timer.ENC_AB, pin=Pin(PIN2))
        
        ##################################################
        
        # Program initialization:
        ## Array that store the current counter value and the previous counter value
        self.Counter = [0,0]
        ## Variable to store the current time value and the previous time value
        self.Time = [0,0]
        ## Variable to store the calculated delta counter value
        self.deltaCounter = 0
        ## Variable to store the correction data for the delta value
        self.deltaCorrection = 0
        ## Variable that store the period of the encoder
        self.Period = PERIOD
        
        print ('EncoderDriver Initialized.')
        
    def update(self, Position):
        ''' @brief              Updates the position of the encoder based on previous position
            @details            Function that updates the encoder position based on its previous position
            @param Position     Previous position of the encoder
        '''
        ## Variable to store the input position
        self.Position = Position
        
        self.Counter.pop(0) # remove the first position of the Counter array
        self.Time.pop(0)# remove the first position of the Time array
        
        self.Counter.append(self.TIMR.counter()) # append the new counter value to the Counter array
        self.Time.append(utime.ticks_ms()) # append the new time value to the Time array
        
        self.deltaCounter = self.Counter[1] - self.Counter[0] # Calculating the difference between the two counter values
        self.deltaTime = self.Time[1] - self.Time[0] # Calculating the difference between the two time values
        
        # Correcting deltaCounter
        if -0.5*self.Period <= self.deltaCounter <= 0.5*self.Period:
            self.deltaCorrection = self.deltaCounter
        elif self.deltaCounter > 0.5*self.Period:
            self.deltaCorrection = self.deltaCounter - self.Period
        elif self.deltaCounter < -0.5*self.Period:
            self.deltaCorrection = self.deltaCounter + self.Period
        
        self.Position += self.deltaCorrection  # store the new corrected position
        
        if self.deltaTime == 0:
            self.omega_means = 0
        else:
            self.omega_means = self.deltaCorrection/self.deltaTime # store the velocity value
            
        return self.Position # return the corrected position and the velocity
        
    def set_position(self,resetPosition):
        ''' @brief                  Reset the position of the encoder to the specified value
            @param resetPosition    Position value that the user wants to be set to
        '''
        return resetPosition
        
    def get_position(self):
        ''' @brief                  Give the current position of the encoder
        '''
        return self.Position
        
    def get_delta(self):
        ''' @brief                  Give the current velocity of the encoder
        '''
        return self.omega_means
''' @file       UI_dataGen1.py
    @brief      Read inputs, collect and return data
    @details    A Nucleo program that read input keyboard data from the computer
                and execute command.
                Here is a snapshot of the finite state machine:
                \image html ME305_Lab0xFF.jpg width=800cm height=600cm
                See file here:
                    https://bitbucket.org/Ahkar/me305_labs/src/master/Lab0xFF/
    @author     Ahkar Kyaw
    @date       2/18/21
    @copyright  2020-2021 Kyaw
'''

import pyb
import utime
from pyb import UART
from array import array
from math import exp, sin, pi


def transitionTo(newState):
    """ @brief          Function to help change state
        @param newState Input variable, new State that we want to move to.
    """
    global currentState
    print('S{:}->S{:}'.format(currentState,newState)) # print change of state in PuTTY
    currentState = newState # update the currentState to newState

def equation(time):
    """ @brief          Equation of the function that we want to obtain the data for
        @param time     Input variable, time at which we want to find the data point for
    """
    value = exp(-time/10)*sin(2*pi/3*time)
    return value


# Main program / test program begin
#   This code only runs if the script is executed as main by pressing play
#   but does not run if the script is imported as a a module
if __name__=="__main__":
    # Constants initialization
    # State constants
    S0_INIT             = 0 ## initialization state
    S1_COLLECTDATA      = 1 ## data collection state
    S2_RETURNDATA       = 2 ## data reporting state
    
    # Program initialization
    ## The current state of the finite state machine
    currentState = S0_INIT
    ## An array for time with initial time value of 0s
    t = array('f',[0])
    ## An array for f(t) with initial value calculated at time = 0s
    y = array('f',[equation(t[0])])
    ## Time Step for data collection [ms]
    stepTime = 50
    ## Tolerance for data collection stepTime [ms]
    stepTol = 5
    ## Lower range value for time data colleciton
    minStep = stepTime-stepTol
    ## Upper range value for time data colleciton
    maxStep = stepTime+stepTol
    
    # Hardware initialization
    myuart = UART(2) ## Store UART object for serial communications protocol
    pyb.repl_uart(None) # prevent print command from printing in Spyder
    
    print("Welcome!\nProgram Initialized.")
    print("Press 'g' to collect data.\nPress 's' to return data.")
    while True:
        try:
            if currentState == S0_INIT:
                if myuart.any() != 0:
                    ## Store input character from Spyder
                    val = myuart.readchar() # store input from Spyder
                    if val == 71 or val == 103: # if input is 'G' or 'g'
                        transitionTo(S1_COLLECTDATA) # updating state for next iteration
                        ## Store the starting time of data collection
                        startTime = utime.ticks_ms()
                    else:
                        pass
            elif currentState == S1_COLLECTDATA:
                if myuart.any() != 0:
                    val = myuart.readchar() # store input from Spyder
                    if val == 83 or val ==115: # if input is 'S' or 's'
                        ## Counter to send data
                        n = 0
                        transitionTo(S2_RETURNDATA) # updating state for next iteration
                    else:
                        pass
                ## Store the current time
                currentTime = utime.ticks_ms()
                ## Store the deltaTime from startTime to currentTime
                deltaTime = utime.ticks_diff(currentTime,startTime)
                print('deltaTime = '+str(deltaTime))
                if deltaTime >= 30000:
                    print('30s worth of data collected.')
                    n = 0
                    transitionTo(S2_RETURNDATA) # updating state for next iteration
                elif minStep <= deltaTime%stepTime <= maxStep:
                    t.append(deltaTime/1000)
                    y.append(equation(deltaTime/1000))
            elif currentState == S2_RETURNDATA:
                print('Sending Data...')
                while n!=len(t):
                    myuart.write('{:},{:}\r\n'.format(t[n],y[n]))
                    n+=1
                myuart.write('Done')
                print('Data Sent')
                transitionTo(S0_INIT) # updating state for next iteration
            else:
                pass
        except KeyboardInterrupt:
            # This except block catches "Ctrl-C" from the keyboard
            # to end the while(True) loop when desired
            print('Ctrl+c has been pressed')
            break


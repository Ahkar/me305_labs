''' @file       MotorDriver4.py
    @brief      Used to correct the Motor
    @details    A class that helps communicate between the motor and the controller task
    @author     Ahkar Kyaw
    @date       3/18/21
    @copyright  2020-2021 Kyaw
'''

import pyb
from pyb import Pin, Timer

class MotorDriver:
    ''' This class implements a motor driver for the ME405 board.'''

    def __init__ (self, nSLEEP_pin, CH1, CH2, IN1_pin, IN2_pin, TIM):
        ''' Creates a motor driver by initializing GPIO pins and turning the motor off for safety.
            @param nSLEEP_pin   A pyb.Pin object to use as the enable pin.
            @param IN1_pin      A pyb.Pin object to use as the input to half bridge 1.
            @param IN2_pin      A pyb.Pin object to use as the input to half bridge 2.
            @param timer        A pyb.Timer object to use for PWM generation on
                                IN1_pin and IN2_pin.'''
        
        # Hardware initialization:
        pyb.repl_uart(None) # prevent print command from printing in Spyder
        
        ## Stores the timer variable
        self.TIM = Timer(TIM, freq=20000)
        ## Variable to store the pin value for nSLEEP
        self.nSLEEP = Pin(nSLEEP_pin, mode=Pin.OUT_PP, value=0)
        ## Variable to store the first input pin for the motor
        self.IN1 = self.TIM.channel(CH1, mode=Timer.PWM, pin=Pin(IN1_pin))
        ## Variable to store the second input pin for the motor
        self.IN2 = self.TIM.channel(CH2, mode=Timer.PWM, pin=Pin(IN2_pin))
        
        print ('MotorDriver Initialized.')
        
    def enable (self):
        ''' @brief      Activate the motor
        '''
        self.nSLEEP.high()
    
    def disable (self):
        ''' @brief      Deactivate the motor
        '''
        self.nSLEEP.low()
    
    def set_duty (self, duty):
        ''' This method sets the duty cycle to be sent
            to the motor to the given level. Positive values
            cause effort in one direction, negative values
            in the opposite direction.
            @param duty A signed integer holding the duty
            cycle of the PWM signal sent to the motor '''
        ## Stores the input duty for calculation
        self.duty = duty
        if self.duty > 0:
            self.IN1.pulse_width_percent(self.duty)
            self.IN2.pulse_width_percent(0)
        elif self.duty < 0:
            self.IN1.pulse_width_percent(0)
            self.IN2.pulse_width_percent(abs(self.duty))
        elif self.duty == 0:
            self.IN1.pulse_width_percent(0)
            self.IN2.pulse_width_percent(0)
    

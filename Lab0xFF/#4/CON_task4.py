''' @file       CON_task4.py
    @brief      Control the EncoderDriver, MotorDriver and ClosedLoop class using finite state machine
    @details    Based on the state in the shared folder, 
                carry out tasks that communicate with the EncoderDriver class, MotorDriver class and ClosedLoop class
    @author     Ahkar Kyaw
    @date       3/18/21
    @copyright  2020-2021 Kyaw
'''

import pyb
import shares
from EncoderDriver4 import EncoderDriver
from MotorDriver4 import MotorDriver
from ClosedLoop4 import ClosedLoop

class CON_task:
    
    # Constants initializationz
    # State constants
    S0_INIT      = 0
    S1_PRESSED_Z = 1
    S2_PRESSED_P = 2
    S3_PRESSED_D = 3
    S4_PRESSED_G = 4
    S5_PRESSED_S = 5
    S6_PRESSED_F = 6
    S7_PRESSED_O = 7
    
    def __init__(self):
        ''' @brief      A class that connects the EncoderDriver, MotorDriver and ClosedLoop together
            @details    Initialize the controller task which will be used to communicate with the three driver classes.
        '''
        # Function initialization:
        
        ##################################################
        
        # Program initialization:
        ## Local counter variable
        self.n = 0
        ## Variable to store the reset value
        self.resetPosition = 0
        ## Variable that store the period of the encoder
        self.Period = 0xFFFF
        
        ##################################################
        
        # Hardware initialization:
        pyb.repl_uart(None) # prevent print command from printing in Spyder
        
        ##################################################
        
        # Encoder
        ## Variable to store the timer value for Encoder 1
        self.Enc1_TIM = 4
        ## Variable to store the first pin value for Encoder 1
        self.Enc1_Pin1 = 'B6'
        ## Variable to store the second pin value for Encoder 1
        self.Enc1_Pin2 = 'B7'
        # self.Enc2_TIM = 8
        # self.Enc2_Pin1 = 'C6'
        # self.Enc2_Pin2 = 'C7'
        
        # EncoderDriver
        ## Encoder driver class variable for encoder 1
        self.Enc1 = EncoderDriver(self.Enc1_Pin1, self.Enc1_Pin2, self.Enc1_TIM, self.Period)
        # self.Enc2 = EncoderDriver(self.Enc2_Pin1, self.Enc2_Pin2, self.Enc2_TIM, self.Period)
        
        ##################################################
        
        # Motor
        ## Variable to store the pin value for nSLEEP
        self.pin_nSLEEP = 'A15'
        ## Variable to store the first channel number for Motor 1
        self.CH1 = 1
        ## Variable to store the second channel number for Motor 1
        self.CH2 = 2
        # self.CH3 = 3
        # self.CH4 = 4
        ## Variable to store the first pin number for Motor 1
        self.IN1 = 'B4'
        ## Variable to store the first pin number for Motor 1
        self.IN2 = 'B5'
        # self.IN3 = 'B0'
        # self.IN4 = 'B1'
        ## Variable to store the timer number for Motor 1
        self.Mot_TIM = 3
        
        # MotorDriver
        ## Motor driver class variable for motor 1
        self.Mot1 = MotorDriver(self.pin_nSLEEP, self.CH1, self.CH2, self.IN1, self.IN2, self.Mot_TIM)
        # self.Mot2 = MotorDriver(self.pin_nSLEEP, self.CH3, self.CH4, self.IN3, self.IN4, self.Mot_TIM)
        self.Mot1.enable() # Enable the motor
        
        ##################################################
        
        ## Variable to store the desired reference veloity for the motor
        self.omega_ref = 3000 #rpm
        ## Variable to store the PWM value for the motor
        self.PWM = 0
        ## Closedloop class variable
        self.Cloop = ClosedLoop(self.PWM)
        self.Cloop.set_Kp(0.00001) # set the Kp value to 0.00001 for testing
        
        ##################################################
        
        # initialization message
        print("CON_task Initialized.")
        
    def run(self):
        ''' @brief      Runs the finite-state machine
            @details    Comprises of different states for Encoder, Motor and Closedloop
        '''
        ## Stores the current position and the velocity of the encoder to be used across different task
        shares.currentPosition,shares.currentVelocity = self.Enc1.update(shares.currentPosition)
            
        if shares.ENCState == self.S0_INIT:
            pass
        elif shares.ENCState == self.S1_PRESSED_Z:
            shares.currentPosition = self.Enc1.set_position(self.resetPosition)
            print('shares.currentPosition            : ',shares.currentPosition)
            shares.ENCState = self.S0_INIT
        elif shares.ENCState == self.S2_PRESSED_P:
            shares.currentPosition = self.Enc1.get_position()
            print('shares.currentPosition           : ',shares.currentPosition)
            shares.ENCState = self.S0_INIT
        elif shares.ENCState == self.S3_PRESSED_D:
            shares.currentVelocity = self.Enc1.get_delta()
            print('shares.currentVelocity           : ',shares.currentVelocity)
            shares.ENCState = self.S0_INIT
            
        if shares.UIState == self.S0_INIT:
            pass
        elif shares.UIState == self.S4_PRESSED_G:
            self.PWM = self.Cloop.update(self.omega_ref,shares.currentVelocity)
            pass
        elif shares.UIState == self.S5_PRESSED_S:
            pass
        
        if shares.MOTState == self.S0_INIT:
            self.Mot1.enable()
        elif shares.MOTState == self.S6_PRESSED_F:
            self.Mot1.set_duty(0)
            self.Mot1.disable()
            shares.MOTState = self.S0_INIT
        elif shares.MOTState == self.S7_PRESSED_O:
            self.Mot1.set_duty(self.PWM)
